### Naming Standards

| English | Example | Description |
|---------|---------|-------------|
| Double Curly Braces | {{some-name}} | Naming isn't set in stone, room for change |
